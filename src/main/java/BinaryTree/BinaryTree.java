package BinaryTree;

import java.util.Comparator;

public class BinaryTree<V> {
    private Node<V> root;
    private int size;

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public Node<V> getRoot(){
        return root;
    }

    public void printSpaces(int lvl){
        for(int i = 0; i < lvl; i++)
        System.out.print("                  ");
    }

    public void showTree(Node node, int lvl) {
        if (node == null)
            return;
        showTree(node.right, lvl + 1);
        if(!node.isDeleted) {
            printSpaces(lvl);
            System.out.print(node.key + " " + node.value + "\n");
        }
        showTree(node.left, lvl + 1);
    }

    public Node<V> get(int key) {
        Node<V> n = root;
        while (n != null) {
            if (key < n.key)
                n = n.left;
            else if (key > n.key)
                n = n.right;
            else
                if(n.isDeleted)
                    return null;
                else
                    return n;
        }
        return null;
    }

    public Node<V> set(int key, V value) {
        Node<V> n = root;
        while (n != null) {
            if (key < n.key)
                n = n.left;
            else if (key > n.key)
                n = n.right;
            else {
                if(n.isDeleted)
                    n.isDeleted = false;
                n.value = value;
                return n;
            }
        }
        return null;
    }

    public void put(int key, V value) {
        Node<V> n = root;
        if (n == null) {
            root = new Node<V>(key, value, null);
            size = 1;
            return;
        }
        Node<V> parent;
        do {
            parent = n;
            if (key < n.key)
                n = n.left;
            else if (key > n.key)
                n = n.right;
            else {
                n.setValue(value);
                return;
            }
        }while (n != null);
        Node<V> newNode = new Node<V>(key, value, parent);
        if(key<parent.key)
            parent.left = newNode;
        else
            parent.right = newNode;
        size++;
    }

    public void remove(int key) {
        Node<V> n = root;
        while (n != null) {
            if (key < n.key)
            n = n.left;
            else if (key > n.key)
                n = n.right;
            else break;
        }
        n.isDeleted = true;
    }

    public void clear() {
        size = 0;
        root = null;
    }

    public V getValue(Node<V> node){
        return node.getValue();
    }
}

class Node<V> {
    boolean isDeleted = false;
    int key;
    V value;
    Node<V> left;
    Node<V> right;
    Node<V> parent;

    public Node(int key, V value, Node<V> parent){
        this.key = key;
        this.value = value;
        this.parent = parent;
    }

    public int getKey(){
        return key;
    }
    public V getValue(){
        return value;
    }
    public void setValue(V value){
        this.value = value;
    }
}
