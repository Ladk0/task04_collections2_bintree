import BinaryTree.BinaryTree;

import java.awt.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    enum Choices {
        add("Add node"), remove("Remove node"), get("Get node"), set("Set node"), print("Print tree"), quit("Quit");

        private String message;

        Choices(String message){
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        BinaryTree<String> myTree = new BinaryTree<String>();
        Choices choice;

        do {
            for(Choices menuChoice : Choices.values())
                System.out.println(menuChoice + " - " + menuChoice.getMessage());
            System.out.println();
            String temp;
            do {
                temp = scanner.nextLine();
            } while(!temp.equals("add") && !temp.equals("remove") && !temp.equals("get") && !temp.equals("set") && !temp.equals("print") && !temp.equals("quit"));
            choice = Choices.valueOf(temp);
            switch(choice){
                case add: {
                    System.out.println("Enter the key and the value of the new node:");
                    myTree.put(scanner.nextInt(), scanner.nextLine());
                    break;
                }
                case remove: {
                    System.out.println("Enter the key of the node to be deleted:");
                    myTree.remove(scanner.nextInt());
                    break;
                }
                case get: {
                    System.out.println("Enter the key of the node to be displayed:");
                    System.out.println(myTree.getValue(myTree.get(scanner.nextInt())));
                    break;
                }
                case set: {
                    System.out.println("Enter the key and the value of the node to be overwritten:");
                    myTree.set(scanner.nextInt(), scanner.nextLine());
                    break;
                }
                case print: {
                    System.out.println("Current tree's structure:");
                    myTree.showTree(myTree.getRoot(), 0);
                    break;
                }
            }
        } while(choice != Choices.quit);
    }
}
